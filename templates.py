##Filename: templates.py

def source(source):
    return {
        'title': source.name,
        'image_url': source.logo_url,
        'buttons': [
            {
                'type': 'postback', 'title': 'Show mangas',
                'payload': 'show_mangas {}'.format(source.id)
            },
            {
                'type': 'web_url', 'title': 'Visit Web',
                'url': source.url
            }
        ]
    }

def manga(source_id, manga):
    url = manga.sources[0].pivot.url

    return {
        'title': manga.name,
        'image_url': manga.image_url,
        'buttons': [
            {
                'type': 'postback', 'title': 'Subscribe',
                'payload': 'subscribe {} {}'.format(source, manga.id)},
            {
                'type': 'web_url', 'title': 'Visit Web', 'url': url},
            {
                'type': 'element_share',
                'share_contents': {
                    'attachment': {
                        'type': 'template',
                        'payload': {
                            'template_type': 'generic',
                            'elements': [{
                                'title': manga.name,
                                'image_url': manga.image_url,
                                'buttons': [{
                                    'type': 'web_url',
                                    'url': url,
                                    'title': 'Take a look to {}'.format(manga.name)
                                }]
                            }]
                        }
                    }
                }
            }
        ]
    }
