## Filename dbhandler.py

import pymysql.cursors

class BotService(object):
    @staticmethod
    def open_db():
        return pymysql.connect(host='localhost',
                           user='root',
                           password='root',
                           db='otamegane',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)

    @staticmethod
    def find_user(chat_id):
        db = BotService.open_db()
        user = None
        try:
            with db.cursor() as cursor:
                sql = "SELECT * FROM messenger_chats WHERE chat_id = %s LIMIT 1;"
                cursor.execute(sql, (chat_id))
                user = cursor.fetchone()
        finally:
            db.close()
        return user

    @staticmethod
    def register_user(chat_id, data_user):
        db = BotService.open_db()
        try:
            with db.cursor() as cursor:
                sql = ("INSERT INTO messenger_chats (chat_id, first_name, last_name, locale, gender, created_at) "
                       "VALUES (%s, %s, %s, %s, %s, NOW());")
                cursor.execute(sql, (
                    chat_id, data_user['first_name'], data_user['last_name'], data_user['locale'], data_user['gender']))
            db.commit()
        finally:
            db.close()

    @staticmethod
    def get_sources():
        db = BotService.open_db()
        sources = dict()
        try:
            with db.cursor() as cursor:
                sql = "SELECT * FROM `sources`;"
                cursor.execute(sql)
                sources = cursor.fetchall()
        finally:
            db.close()
        return sources

    @staticmethod
    def get_mangas(source):
        db = BotService.open_db()
        mangas = dict()
        try:
            with db.cursor() as cursor:
                sql = ("SELECT mangas.id as id, mangas.name, mangas.image_url, manga_source.url "
                       "FROM mangas JOIN manga_source ON mangas.id = manga_source.manga_id "
                       "WHERE source_id = %s;")
                cursor.execute(sql, (source))
                mangas = cursor.fetchall()
        finally:
            db.close()
        return mangas

    @staticmethod
    def find_source(source_id):
        db = BotService.open_db()
        source = None
        try:
            with db.cursor() as cursor:
                sql = "SELECT * FROM sources WHERE id = %s LIMIT 1;"
                cursor.execute(sql, (source_id))
                source = cursor.fetchone()
        finally:
            db.close()
        return source

    @staticmethod
    def find_source_by_name(source_name):
        db = BotService.open_db()
        source = None
        try:
            with db.cursor() as cursor:
                sql = "SELECT * FROM sources WHERE name = %s LIMIT 1;"
                cursor.execute(sql, (source_name))
                source = cursor.fetchone()
        finally:
            db.close()
        return source

    @staticmethod
    def find_manga(manga_id):
        db = BotService.open_db()
        manga = None
        try:
            with db.cursor() as cursor:
                sql = "SELECT * FROM mangas WHERE id = %s LIMIT 1;"
                cursor.execute(sql, (manga_id))
                manga = cursor.fetchone()
        finally:
            db.close()
        return manga

    @staticmethod
    def find_manga_by_name(manga_name):
        db = BotService.open_db()
        manga = None
        try:
            with db.cursor() as cursor:
                sql = "SELECT * FROM mangas WHERE name = %s LIMIT 1;"
                cursor.execute(sql, (manga_name))
                manga = cursor.fetchone()
        finally:
            db.close()
        return manga

    @staticmethod
    def find_manga_source(source_id, manga_id):
        db = BotService.open_db()
        manga_source = None
        try:
            with db.cursor() as cursor:
                sql = (
                    "SELECT manga_source.id, mangas.name as manga_name, sources.name as source_name "
                    "FROM manga_source "
                    "JOIN mangas ON manga_source.manga_id = mangas.id "
                    "JOIN sources ON manga_source.source_id = sources.id "
                    "WHERE source_id = %s AND manga_id = %s LIMIT 1;")
                cursor.execute(sql, (source_id, manga_id))
                manga_source = cursor.fetchone()
        finally:
            db.close()
        return manga_source

    @staticmethod
    def find_manga_source_by_names(source_name, manga_name):
        db = BotService.open_db()
        manga_source = None
        try:
            with db.cursor() as cursor:
                sql = (
                    "SELECT manga_source.id FROM manga_source "
                    "JOIN mangas ON manga_source.manga_id = mangas.id "
                    "JOIN sources ON manga_source.source_id = sources.id "
                    "WHERE sources.name = %s AND mangas.name = %s LIMIT 1;")
                cursor.execute(sql, (source_name, manga_name))
                manga_source = cursor.fetchone()
        finally:
            db.close()
        return manga_source

    @staticmethod
    def already_subscribed(manga_source_id, chat_id):
        db = BotService.open_db()
        subscription = None
        try:
            with db.cursor() as cursor:
                sql = (
                    "SELECT id FROM subscriptions "
                    "WHERE manga_source_id = %s AND messenger_chat_id = %s;")
                cursor.execute(sql, (manga_source_id, chat_id))
                subscription = cursor.fetchone()
        finally:
            db.close()
        return subscription is not None

    @staticmethod
    def subscribe(manga_source_id, chat_id):
        # For the moment, only messenger
        db = BotService.open_db()
        try:
            with db.cursor() as cursor:
                sql = "INSERT INTO subscriptions (manga_source_id, messenger_chat_id, created_at) VALUES (%s, %s, NOW());"
                cursor.execute(sql, (manga_source_id, chat_id))
            db.commit()
        finally:
            db.close()

    @staticmethod
    def get_subscriptions(chat_id):
        db = BotService.open_db()
        subscriptions = []
        try:
            with db.cursor() as cursor:
                sql = (
                    "SELECT subscriptions.id as id, manga_source.url, mangas.name as manga_name, mangas.image_url, sources.name as source_name "
                    "FROM subscriptions "
                    "JOIN manga_source ON subscriptions.manga_source_id = manga_source.id "
                    "JOIN mangas ON manga_source.manga_id = mangas.id "
                    "JOIN sources ON manga_source.source_id = sources.id "
                    "WHERE messenger_chat_id = %s;")
                cursor.execute(sql, (chat_id))
                subscriptions = cursor.fetchall()
        finally:
            db.close()
        return subscriptions

    @staticmethod
    def get_last_notification(source, manga):
        db = BotService.open_db()
        notification = None
        try:
            with db.cursor() as cursor:
                sql = ("SELECT * FROM notifications "
                       "WHERE source = %s AND manga = %s ORDER BY created_at DESC LIMIT 1;")
                cursor.execute(sql, (source, manga))
                notification = cursor.fetchone()
        finally:
            db.close()
        return notification

    @staticmethod
    def get_manga_source_of_subscription(subscription_id):
        db = BotService.open_db()
        subscription = None
        try:
            with db.cursor() as cursor:
                sql = ("SELECT mangas.name as manga, sources.name as source "
                        "FROM subscriptions "
                        "JOIN manga_source ON subscriptions.manga_source_id = manga_source.id "
                        "JOIN mangas ON manga_source.manga_id = mangas.id "
                        "JOIN sources ON manga_source.source_id = sources.id "
                        "WHERE subscriptions.id = %s LIMIT 1;")
                cursor.execute(sql, (subscription_id))
                subscription = cursor.fetchone()
        finally:
            db.close()
        return subscription

    @staticmethod
    def unsubscribe(subscription_id):
        db = BotService.open_db()
        try:
            with db.cursor() as cursor:
                sql = "DELETE FROM subscriptions WHERE id = %s;"
                cursor.execute(sql, (subscription_id))
            db.commit()
        finally:
            db.close()

    @staticmethod
    def first_or_create_notification(manga, source):
        db = BotService.open_db()
        notification_id = None
        try:
            with db.cursor() as cursor:
                sql = ("SELECT * FROM notifications "
                       "WHERE source = %s AND manga = %s AND chapter = %s"
                       "ORDER BY created_at DESC LIMIT 1;")
                cursor.execute(sql, (source['name'], manga['name'], manga['chapter']))
                notification = cursor.fetchone()

                if notification is None:
                    sql = ("INSERT INTO notifications (manga, chapter, title, url, source, created_at) "
                        "VALUES(%s, %s, %s, %s, %s, NOW())")
                    cursor.execute(sql, (manga['name'], manga['chapter'],
                        manga['title'], manga['url'], source['name']))
                    notification_id = cursor.lastrowid
                else:
                    notification_id = notification['id']
            db.commit()
        finally:
            db.close()
        return notification_id

    @staticmethod
    def get_subscribers(source, manga, notification_id):
        db = BotService.open_db()
        subscribers = []
        manga_source = BotService.find_manga_source_by_names(source, manga)

        if manga_source is None:
            return subscribers

        try:
            with db.cursor() as cursor:
                sql = ( "SELECT * FROM subscriptions "
                        "WHERE subscriptions.messenger_chat_id NOT IN( "
                            "SELECT messenger_chat_id FROM log_notifications "
                            "WHERE notification_id = %s "
                        ") AND subscriptions.manga_source_id = %s;")
                cursor.execute(sql, (notification_id, manga_source['id']))
                subscribers = cursor.fetchall()
        finally:
            db.close()
        return subscribers

    @staticmethod
    def save_log_notification(subscriber_chat_id, notification_id):
        db = BotService.open_db()
        try:
            with db.cursor() as cursor:
                sql = "INSERT INTO log_notifications (notification_id, messenger_chat_id) VALUES (%s, %s);"
                cursor.execute(sql, (notification_id, subscriber_chat_id))
            db.commit()
        finally:
            db.close()
