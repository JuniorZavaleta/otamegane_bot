from orator.migrations import Migration

class CreateMangasTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('mangas') as table:
            table.increments('id')
            table.string('name', 100)
            table.string('image_url', 100)
            table.timestamps()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('mangas')
