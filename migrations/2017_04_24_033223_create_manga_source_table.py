from orator.migrations import Migration


class CreateMangaSourceTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('manga_source') as table:
            table.increments('id')
            table.integer('manga_id').unsigned()
            table.integer('source_id').unsigned()
            table.string('url', 100)

            table.foreign('manga_id').references('id').on('mangas')
            table.foreign('source_id').references('id').on('sources')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('manga_source')
