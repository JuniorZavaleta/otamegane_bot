from orator.migrations import Migration

class CreateSourcesTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('sources') as table:
            table.increments('id')
            table.string('url', 100)
            table.string('name', 50)
            table.string('logo_url', 100)
            table.timestamps()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('sources')
