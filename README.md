# OtameganeBot

Clone the project

```
git clone https://gitlab.com/JuniorZavaleta/otamegane_bot.git
```

Pip and lib for mysql with python and virtualenv

```
sudo apt-get install python-pip python-dev libmysqlclient-dev virtualenv
```

Go to the directory

```
cd otamegane_bot
```

Create a virtual env
```
virtualenv venv --distribute
```

Activate the virtual env
```
source venv/bin/activate
```

Packages for scrapping
```
sudo apt-get install python-dev libxml2-dev libxslt1-dev zlib1g-dev libffi-dev libssl-dev
```

Install the dependencies
```
pip install -r requirements.txt
```

Use ngrok and configure your webhook on developers facebook dashboard
