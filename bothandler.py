## Filename bothandler.py
import requests
from messenger import Messenger
from botservice import BotService
from models import Manga, Source
import templates

class BotHandler(object):
    chat_id = None

    """docstring for ClassName"""
    def __init__(self, chat_id):
        self.chat_id = chat_id

    def handle_bot(self, messaging):
        if 'postback' in messaging:
            self.handle_postback(messaging['postback']['payload'])
        elif 'message' in messaging:
            self.handle_message(messaging['message'])
        return 'ok'

    def handle_postback(self, payload):
        if payload == 'show_sources':
            self.show_sources()
        elif payload == 'my_mangas':
            self.my_mangas()
        elif payload == 'start':
            self.start()

        payload = payload.split()
        if payload[0] == 'show_mangas':
            self.show_mangas(payload[1])
        elif payload[0] == 'subscribe':
            self.ask_for_subscription(payload[1], payload[2])
        elif payload[0] == 'unsubscribe':
            self.ask_for_unsubscription(payload[1])

        return 'ok'

    def handle_message(self, message):
        if 'quick_reply' in message:
            payload = message['quick_reply']['payload']
            payload = payload.split()

            if payload[0] == 'subscribe' and payload[3] == 'YES':
                self.subscribe(payload[1], payload[2])
            if payload[0] == 'unsubscribe' and payload[2] == 'YES':
                self.unsubscribe(payload[1])
        elif 'message' in message:
            return 'ok'

    def start(self):
        user_data = BotService.find_user(self.chat_id)
        if user_data is None:
            user_data = Messenger.get_user_data(self.chat_id)
            BotService.register_user(self.chat_id, user_data)
            Messenger.send_text(self.chat_id, 'Hi {}, I am OtameganeBot. Please use the menu.'.format(user_data['first_name']))
        else:
            Messenger.send_text(self.chat_id, 'Hi again {}. Please use the menu.'.format(user_data['first_name']))

    def show_sources(self):
        data = []
        for source in Source.all():
            data.append(templates.source(source))

        Messenger.send_template(self.chat_id, data)

    def show_mangas(self, source_id):
        data = []
        mangas = Manga.with_('sources').where_has('sources',
            lambda q: q.where('source_id', source_id)
        ).get()

        for manga in mangas:
            data.append(templates.manga(source_id, manga))

        Messenger.send_template(self.chat_id, data)

    def ask_for_subscription(self, source_id, manga_id):
        manga_source = BotService.find_manga_source(source_id, manga_id)

        if BotService.already_subscribed(manga_source['id'], self.chat_id):
            Messenger.send_text(self.chat_id, "You are already subscribed.")
        else:
            manga = BotService.find_manga(manga_id)
            source = BotService.find_source(source_id)
            message = "Do you want subscribe to {} from {}?".format(
                manga['name'], source['name'])

            quick_replies = [
                {'content_type': 'text', 'title': 'Yes, I want it', 'payload': 'subscribe {} {} YES'.format(source_id, manga_id)},
                {'content_type': 'text', 'title': 'No, thanks', 'payload': 'show_mangas {}'.format(source_id)}
            ]
            Messenger.send_text(self.chat_id, message, quick_replies)

    def subscribe(self, source_id, manga_id):
        manga_source = BotService.find_manga_source(source_id, manga_id)
        if BotService.already_subscribed(manga_source['id'], self.chat_id):
            Messenger.send_text(self.chat_id, "You are already subscribed.")
        else:
            manga_source = BotService.find_manga_source(source_id, manga_id)

            BotService.subscribe(manga_source['id'], self.chat_id)
            Messenger.send_text(self.chat_id,
                "Manga {} of {} subscribed successfully :D".format(
                    manga_source['manga_name'], manga_source['source_name']))

    def my_mangas(self):
        subscriptions = BotService.get_subscriptions(self.chat_id)

        data = []
        for subscription in subscriptions:
            new_element = {
                'title': subscription['manga_name'],
                'subtitle': subscription['source_name'],
                'image_url': subscription['image_url'],
                'buttons': [
                    {'title': 'Unsubscribe', 'type': 'postback', 'payload': 'unsubscribe {}'.format(subscription['id'])}
                ]
            }
            notification = BotService.get_last_notification(subscription['source_name'], subscription['manga_name'])
            if notification is not None:
                new_element['buttons'].append({
                    'title': 'See last chapter',
                    'type': 'web_url',
                    'url': notification['url']
                })
                new_element['buttons'].append({
                    'type': 'element_share',
                    'share_contents': {
                        'attachment': {
                            'type': 'template',
                            'payload': {
                                'template_type': 'generic',
                                'elements': [{
                                    'title': subscription['manga_name'],
                                    'image_url': subscription['image_url'],
                                    'buttons': [{
                                        'type': 'web_url',
                                        'url': notification['url'],
                                        'title': 'See last chapter'
                                    }]
                                }]
                            }
                        }
                    }
                })
            else:
                new_element['buttons'].append({
                    'title': 'Visit Web',
                    'type': 'web_url',
                    'url': subscription['url']
                })
                new_element['buttons'].append({
                    'type': 'element_share',
                    'share_contents': {
                        'attachment': {
                            'type': 'template',
                            'payload': {
                                'template_type': 'generic',
                                'elements': [{
                                    'title': subscription['manga_name'],
                                    'image_url': subscription['image_url'],
                                    'buttons': [{
                                        'type': 'web_url',
                                        'url': subscription['url'],
                                        'title': 'Take a look to {}'.format(subscription['manga_name'])
                                    }]
                                }]
                            }
                        }
                    }
                })

            data.append(new_element)

        Messenger.send_template(self.chat_id, data)

    def ask_for_unsubscription(self, subscription_id):
        subscription = BotService.get_manga_source_of_subscription(subscription_id)

        if subscription is None:
            message = ':|'

            Messenger.send_text(self.chat_id, message)
        else:
            message = "Are you sure unsubscribe {} from {}?".format(
            subscription['manga'], subscription['source'])

            quick_replies = [
                {'content_type': 'text', 'title': 'Yes', 'payload': 'unsubscribe {} YES'.format(subscription_id)},
                {'content_type': 'text', 'title': 'No, back', 'payload': 'my_mangas'}
            ]

            Messenger.send_text(self.chat_id, message, quick_replies)

    def unsubscribe(self, subscription_id):
        subscription = BotService.get_manga_source_of_subscription(subscription_id)

        if subscription is None:
            message = ':|'
        else:
            message = "Manga {} of {} unsubscribe successfully".format(
                subscription['manga'], subscription['source'])
            BotService.unsubscribe(subscription_id)

        Messenger.send_text(self.chat_id, message)
