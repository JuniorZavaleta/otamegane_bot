# Filename mangastream_scrapper.py
from manga_scrapper import MangaScrapper

class MangaStreamScrapper(MangaScrapper):
    source_name = 'MangaStream'

    def __init__(self):
        super(MangaStreamScrapper, self).__init__()

    def filter(self, crawler):
        return crawler.find('ul', class_='new-list').find_all('li')

    def get_manga(self, item):
        return {
            'time': item.find('span').find(text=True),
            'url': item.find('a').get('href'),
            'title': item.find('em').find(text=True),
            'chapter': item.find('strong').find(text=True),
            'name': self.name(item)
        }

    def is_recent(self, time):
        return time == 'Today'

    def name(self, item):
        return list(item.strings)[1].strip()
