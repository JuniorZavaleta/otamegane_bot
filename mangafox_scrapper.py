# Filename mangafox_scrapper.py
from manga_scrapper import MangaScrapper

class MangaFoxScrapper(MangaScrapper):
    source_name = 'MangaFox'

    def __init__(self):
        super(MangaFoxScrapper, self).__init__()

    def filter(self, crawler):
        return crawler.find('ul', id='updates').find_all('li')

    def get_manga(self, item):
        title = item.find('a', class_='series_preview').find(text=True)
        a_chapter = item.find('a', class_='chapter')

        return {
            'time': item.find('em').find(text=True),
            'url': a_chapter.get('href'),
            'title': title,
            'chapter': a_chapter.find(text=True)[len(title):].strip(),
            'name': title
        }

    def is_recent(self, time):
        return ('minute' in time) or ('hour' in time) or (time == 'Today')
