# Filename manga_scrapper.py
from abc import ABCMeta, abstractmethod, abstractproperty

import urllib2
from bs4 import BeautifulSoup

from botservice import BotService
from messenger import Messenger

class MangaScrapper(object):
    __metaclass__ = ABCMeta

    source = None
    mangas = None

    @abstractproperty
    def source_name(self):
        pass

    def __init__(self):
        self.source = BotService.find_source_by_name(self.source_name)
        self.mangas = BotService.get_mangas(self.source['id'])

    def scrapping(self):
        page = urllib2.urlopen(self.source['url'])
        crawler = BeautifulSoup(page, "lxml")
        manga_list = self.filter(crawler)

        for item in manga_list:
            manga = self.get_manga(item)

            if self.is_recent(manga['time']):
                notification_id = BotService.first_or_create_notification(manga, self.source)
                subscribers = BotService.get_subscribers(self.source['name'], manga['name'], notification_id)
                if len(subscribers) > 0:
                    manga_db = BotService.find_manga_by_name(manga['name'])
                    for subscriber in subscribers:
                        data = [
                            {
                                'title': '{} - {}'.format(manga['name'], manga['chapter']),
                                'subtitle': manga['title'],
                                'image_url': manga_db['image_url'],
                                'buttons': [
                                    {
                                        'type': 'web_url',
                                        'url': manga['url'],
                                        'title': 'See Chapter'
                                    },
                                    {
                                        'type': 'element_share',
                                        'share_contents': {
                                            'attachment': {
                                                'type': 'template',
                                                'payload': {
                                                    'template_type': 'generic',
                                                    'elements': [{
                                                        'title': manga['name'],
                                                        'image_url': manga_db['image_url'],
                                                        'buttons': [{
                                                            'type': 'web_url',
                                                            'url': manga['url'],
                                                            'title': 'See last chapter'
                                                        }]
                                                    }]
                                                }
                                            }
                                        }
                                    }]
                            }
                        ]
                        Messenger.send_template(subscriber['messenger_chat_id'], data)
                        BotService.save_log_notification(subscriber['messenger_chat_id'], notification_id)
            else:
                break

    @abstractmethod
    def filter(self, crawler):
        pass

    @abstractmethod
    def get_manga(self, item):
        pass

    @abstractmethod
    def is_recent(self, time):
        pass
