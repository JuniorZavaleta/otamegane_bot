from orator import DatabaseManager, Model
from orator.orm import belongs_to_many

config = {
    'mysql': {
        'driver': 'mysql',
        'host': 'localhost',
        'database': 'otamegane',
        'user': 'root',
        'password': 'root',
        'prefix': ''
    }
}

db = DatabaseManager(config)
Model.set_connection_resolver(db)

class Manga(Model):
    __guarded__ = []

    @belongs_to_many('manga_source', with_pivot=['url'])
    def sources(self):
        return Source

class Source(Model):
    __guarded__ = []

    @belongs_to_many('manga_source', with_pivot=['url'])
    def mangas(self):
        return Manga
