## Filename: bot.py
import os
from flask import Flask, request
from bothandler import BotHandler

app = Flask(__name__)

@app.route("/")
def hello():
    return "<h1>It works!</h1>"

@app.route('/messenger', methods=['GET'])
def handle_verification():
    return request.args['hub.challenge']

@app.route('/messenger', methods=['POST'])
def handle_incoming_requests():
    data = request.json
    messaging = data['entry'][0]['messaging'][0]
    chat_id = messaging['sender']['id']
    handler = BotHandler(chat_id)

    return handler.handle_bot(messaging)

if __name__ == '__main__':
    app.run(host='0.0.0.0')