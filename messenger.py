##Filename: messenger.py

import requests

class Messenger(object):
    token = 'EAAGjBhqRoBMBAHQMtE1UzcQbo9MfLFGTgd7bZAUoMZAnOLYLChjkFdkdj3qHvhdjPxpXNfXiJAzNmtaZA3NLP8U50ab8gCZCmWR8WLkMNZBK6ZBrRh1S1zOgqYQCZCw2iSKxGB5zWn29gScGKo34BqbenEBBMt5qJzgj75ltycbogZDZD'

    @staticmethod
    def send_text(chat_id, text, quick_replies = []):
        message = {
            'text': text
        }

        if (quick_replies):
            message.update({
                'quick_replies': quick_replies
            })

        data = {
            'recipient': {'id': chat_id},
            'message': message
        }

        Messenger.send_message(data)

    @staticmethod
    def send_template(chat_id, elements):
        data = {
            'recipient': {'id': chat_id},
            'message': {
                'attachment': {
                    'type': 'template',
                    'payload': {
                        'template_type': 'generic',
                        'elements': elements
                    }
                }
            }
        }

        Messenger.send_message(data)

    @staticmethod
    def send_message(data):
        requests.post('https://graph.facebook.com/v2.6/me/messages?access_token=' + Messenger.token,
                         json=data)

    @staticmethod
    def get_user_data(chat_id):
        return requests.get('https://graph.facebook.com/v2.6/{}?access_token={}'.format(
            chat_id, Messenger.token)).json()
