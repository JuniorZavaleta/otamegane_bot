from orator.seeds import Seeder

from models.source import Source

class SourceTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        Source.create({
            'url': 'https://mangastream.com',
            'name': 'MangaStream',
            'logo_url': 'https://preview.ibb.co/kokjyv/onepiece.jpg'
        })
