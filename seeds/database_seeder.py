from orator.seeds import Seeder
import sys, inspect, os

from seeds.source_table_seeder import SourceTableSeeder
from seeds.manga_table_seeder import MangaTableSeeder

class DatabaseSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        self.call(SourceTableSeeder)
        self.call(MangaTableSeeder)
        pass
