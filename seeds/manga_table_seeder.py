from orator.seeds import Seeder

from models.manga import Manga

class MangaTableSeeder(Seeder):

    def run(self):
        """
        Run the database seeds.
        """
        Manga.create({
            'name': 'One Piece',
            'image_url': 'https://preview.ibb.co/kokjyv/onepiece.jpg'
        })
